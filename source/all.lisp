;;;; Copyright (c) 2017, Philipp Matthias Schäfer (philipp.matthias.schaefer@posteo.de)
;;;;
;;;; This file is part of the Clean SHA3 library.
;;;;
;;;; The Clean Test library is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU Affero General Public License as
;;;; published by the Free Software Foundation, either version 3 of the License
;;;; or (at your option) any later version.
;;;;
;;;; The Clean SHA3 library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
;;;; General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Affero Public License
;;;; along with the Clean SHA3 library. If not, see
;;;; <https://www.gnu.org/licenses/>.

(uiop:define-package :clean-sha3/source/all
  (:nicknames :clean-sha3)
  (:use :clean-util
        :common-lisp
        :sb-rotate-byte)
  (:export :sha3-224
           :sha3-256
           :sha3-384
           :sha3-512))

(in-package :clean-sha3/source/all)

;;; Optimization Parameter

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defparameter *optimization-settings*
    '(optimize (speed 3) (space 0) (safety 0) (debug 0) (compilation-speed 0))))

;;; Constants

(defconstant +sheet-count+ 5)
(defconstant +plane-count+ 5)
(defconstant +lane-count+ 25)
(defconstant +state-bytes+ 200)

;;; Types

(deftype octet () '(unsigned-byte 8))
(deftype octet-vector () '(vector octet))

(deftype lane () '(unsigned-byte 64))
(deftype lanes (count) `(simple-array lane (,count)))
(deftype state () `(lanes ,+lane-count+))

;;; Utilities

(defun make-octet-vector (dimension &rest rest &key &allow-other-keys)
  (apply #'make-array dimension :element-type 'octet rest))

(declaim (inline lane-offset))
(defun lane-offset (x y)
  (+ x (* +sheet-count+ y)))

(declaim (inline lane))
(defun lane (state x y)
  (aref state (lane-offset x y)))

(declaim (inline (setf lane)))
(defun (setf lane) (lane state x y)
  (setf (aref state (lane-offset x y)) lane))

(defun make-empty-state ()
  (make-array +lane-count+ :element-type 'lane :initial-element 0))

(declaim (inline state-octet))
(defun state-octet (state index)
  (ldb (byte 8 (* (mod index 8) 8))
       (aref state (floor index 8))))

(declaim (inline (setf state-octet)))
(defun (setf state-octet) (octet state index)
  (setf (ldb (byte 8 (* (mod index 8) 8))
             (aref state (floor index 8)))
        octet))

(defmacro dotimes-unrolled ((var count) &body body)
  (loop :for i :from 0 :below (eval count)
     :collect `(symbol-macrolet ((,var ,i)) ,@body) :into forms
     :finally (return `(progn ,@forms))))

;;; KECCAK-State Object

(defstruct keccak-state
  (state (make-empty-state) :type state)
  (temp-state (make-empty-state) :type state)
  (temp-lanes (make-array +sheet-count+
                          :element-type 'lane
                          :initial-element 0)
              :type (lanes 5)))

;;; Algorithm Constants

(declaim (type (simple-array (integer 0 63) (25)) +ρ-offsets+))
(defparameter +ρ-offsets+
  (make-array 25 :element-type '(integer 0 63)
              :initial-contents '(0 36 3 41 18
                                  1 44 10  45 2
                                  62 6 43 15 61
                                  28 55 25 21 56
                                  27 20 39 8 14)))

(defun rc (a)
  (declare (type (integer 0 254) a))
  (if (= a 0)
      1
      (let ((r #b00000001))
        (dotimes (i a)
          (setf r (ash r 1))
          (mapc (lambda (i)
                  (setf (ldb (byte 1 i) r)
                        (logxor (ldb (byte 1 i) r)
                                (ldb (byte 1 8) r))))
                '(0 4 5 6))
          (setf r (ldb (byte 8 0) r)))
        (ldb (byte 1 0) r))))

(defun rc_full (i)
  (let ((rc 0))
    (dotimes (j (1+ 6))
      (setf (ldb (byte 1 (1- (expt 2 j))) rc)
            (rc (mod (+ j (* 7 i)) 255))))
    rc))

(declaim (type (lanes 24) +RC+))
(defparameter +RC+
  (make-array 24 :element-type 'lane
              :initial-contents (loop :for i :from 0 :to 23
                                   :collect (rc_full i))))

;;; Round Function

(defun rnd (kstate i)
  (declare #.*optimization-settings*)
  (let ((temp-lanes (keccak-state-temp-lanes kstate))
        (state (keccak-state-state kstate))
        (temp-state (keccak-state-temp-state kstate)))
    ;; PHI
    (dotimes-unrolled (x +sheet-count+)
      (setf (aref temp-lanes x)
            (logxor (lane state x 0)
                    (lane state x 1)
                    (lane state x 2)
                    (lane state x 3)
                    (lane state x 4))))
    (dotimes (x +sheet-count+)
      (let ((d (logxor (aref temp-lanes (mod (+ x 4) 5)) ;; -1 = 4 mod 5
                       (rotate-byte
                        1
                        (byte 64 0)
                        (aref temp-lanes (mod (1+ x) 5))))))
        (dotimes-unrolled (y +plane-count+)
          (setf (lane state x y)
                (logxor (lane state x y) d)))))
    ;; RHO + PI
    (let ((offsets +ρ-offsets+))
      (dotimes (x +sheet-count+)
        (dotimes-unrolled (y +plane-count+)
          (setf (lane temp-state y (mod (+ (* 2 x) (* 3 y)) 5))
                (rotate-byte (aref offsets (+ y (* +plane-count+ x)))
                             (byte 64 0)
                             (lane state x y)))))))
  ;; Swap States
  (let ((temp (keccak-state-state kstate)))
    (setf (keccak-state-state kstate) (keccak-state-temp-state kstate)
          (keccak-state-temp-state kstate) temp))
  (let ((temp-lanes (keccak-state-temp-lanes kstate))
        (state (keccak-state-state kstate)))
    ;; CHI
    (dotimes (y +plane-count+)
      (dotimes-unrolled (x +sheet-count+)
        (setf (aref temp-lanes x)
              (logxor
               (logandc1 (lane state (mod (1+ x) 5) y)
                         (lane state (mod (+ 2 x) 5) y))
               (lane state x y))))
      (dotimes-unrolled (x +sheet-count+)
        (setf (lane state x y) (aref temp-lanes x))))
    ;; IOTA
    (setf (lane state 0 0) (logxor (lane state 0 0) (aref +RC+ i)))
    (values)))

;;; Keccak

(defun keccak (rate-bytes digest-bytes input)
  (declare (type (member 144 136 104 72) rate-bytes)
           (type (member 28 32 48 64) digest-bytes)
           (type (simple-array octet (*)) input)
           #.*optimization-settings*)
  (let* ((padded-bytes (roundup (1+ (length input)) rate-bytes))
         (iterations (ceiling padded-bytes rate-bytes))
         (kstate (make-keccak-state)))
    (symbol-macrolet ((state (keccak-state-state kstate)))
      (dotimes (i (1- iterations))
        (dotimes (j (/ rate-bytes 8))
          (declare (type (integer 0 18) j))
          (setf (aref state j)
                (logxor (aref state j)
                        .
                        #.(loop :for k :from 0 :below 8
                             :collect `(the lane (ash (aref input (+ (* i rate-bytes) (* j 8) ,k))
                                                      (* 8 ,k)))))))
        (dotimes (i 24)
          (rnd kstate i)))
      (let ((rest-bytes (mod (length input) rate-bytes)))
        (dotimes (j rest-bytes)
          (setf (state-octet state j)
                (logxor (state-octet state j)
                        (aref input (+ (* (1- iterations) rate-bytes) j)))))
        ;; suffix + start of padding
        (setf (state-octet state rest-bytes)
              (logxor (state-octet state rest-bytes)
                      #b00000110))
        ;; end of padding
        (setf (state-octet state (1- rate-bytes))
              (logxor (state-octet state (1- rate-bytes))
                      #b10000000))
        (dotimes (i 24)
          (rnd kstate i)))
      (let-return (digest (make-octet-vector digest-bytes))
        (dotimes (i digest-bytes)
          (setf (aref digest i)
                (state-octet state i)))))))


;;; Public Interface

(macrolet ((def-sha3 (digest-bits)
             (let ((docstring (format nil "Calculate the ~A-Bit SHA3-digest for INPUT.

Parameters:

INPUT ((VECTOR OCTET))" digest-bits)))
               (with-gensyms (input digest-bytes rate-bytes)
                 `(defun ,(symbolicate "SHA3-" (write-to-string digest-bits)) (,input)
                    ,docstring
                    (check-type ,input (vector octet))
                    (let* ((,digest-bytes (truncate ,digest-bits 8))
                           (,rate-bytes (- +state-bytes+ (* 2 ,digest-bytes))))
                      (keccak ,rate-bytes ,digest-bytes ,input)))))))
  (def-sha3 224)
  (def-sha3 256)
  (def-sha3 384)
  (def-sha3 512))
