;;;; Copyright (c) 2017, Philipp Matthias Schäfer (philipp.matthias.schaefer@posteo.de)
;;;;
;;;; This file is part of the Clean SHA3 library.
;;;;
;;;; The Clean Test library is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU Affero General Public License as
;;;; published by the Free Software Foundation, either version 3 of the License
;;;; or (at your option) any later version.
;;;;
;;;; The Clean SHA3 library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
;;;; General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Affero Public License
;;;; along with the Clean SHA3 library. If not, see
;;;; <https://www.gnu.org/licenses/>.

(in-package :asdf-user)

#-asdf3.1 (error "Clean Test requires ASDF 3.1 or later. Please upgrade your ASDF.")

(defsystem :clean-sha3
  :description "SHA3 Implementation"
  :long-description #.(read-file-string "README.md")
  :author "Philipp Matthias Schäfer <philipp.matthias.schaefer>"
  :licence "GNU Affero General Public License Version 3"
  :version "1.0.0"
  :class :package-inferred-system
  :depends-on (:clean-sha3/source/all)
  :in-order-to ((test-op (load-op :clean-sha3/tests/all)))
  :perform (test-op (o c) (progn
                            (load-system :clean-standard-out-test-reporter)
                            (symbol-call :clean-test :run-all))))

;;; Systems
(register-system-packages :clean-sha3/source/all
                          '(:clean-sha3/source/all))
(register-system-packages :clean-util '(:clean-util))
(register-system-packages :sb-rotate-byte '(:sb-rotate-byte))

;;; Additional Systems for Tests
(register-system-packages :clean-test '(:clean-test))
(register-system-packages :clean-test/tests/all
                          '(:clean-test/tests/all))
