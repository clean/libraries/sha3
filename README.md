# Clean SHA3 Library

The Clean SHA3 library provides an implementation of SHA3. The
[specification][1] can be obtained free of charge from the NIST website.

The library is tested against [sample data][2] from [NIST][3] retrieved on the
February 7th 2017. The data is contained in the files with ending `rsp` located
in the `tests` subdirectory. Some file's headers where supplemented by a line
present in the other files, so that all headers have equal length.

[1]: http://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.202.pdf.
[2]: http://csrc.nist.gov/groups/STM/cavp/documents/sha3/sha-3bytetestvectors.zip
[3]: http://csrc.nist.gov/groups/STM/cavp/secure-hashing.html#test-vectors

## Warning

Note that the author has very little experience writing code used for
cryptographic purposes.. Therefore, do not use this code in any piece of
software that has to be secure.

## Clean Project

This library is part of the [Clean Project][4], the outlet for my NIH syndrome.

[4]: https://gitlab.com/clean

## License

This library was written by Philipp Matthias Schäfer
(philipp.matthias.schaefer@posteo.de) and is published under the AGPL3 license.
See [LICENSE](LICENSE) for a copy of that license.
