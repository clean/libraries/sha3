;;;; Copyright (c) 2017, Philipp Matthias Schäfer (philipp.matthias.schaefer@posteo.de)
;;;;
;;;; This file is part of the Clean SHA3 library.
;;;;
;;;; The Clean Test library is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU Affero General Public License as
;;;; published by the Free Software Foundation, either version 3 of the License
;;;; or (at your option) any later version.
;;;;
;;;; The Clean SHA3 library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
;;;; General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Affero Public License
;;;; along with the Clean SHA3 library. If not, see
;;;; <https://www.gnu.org/licenses/>.

(defpackage :clean-sha3/tests/all
  (:use :common-lisp
        :clean-sha3
        :clean-test
        :clean-util))

(in-package :clean-sha3/tests/all)

;;;; Utilities for reading the test input-output-pairs.

(defun parse-hex-string (string)
  (loop
     :with length = (length string)
     :for i :from 0 :below length :by 2
     :collect (parse-integer string :start i :end (+ i 2) :radix 16) :into bytes
     :finally (return (make-array (/2 length)
                                  :element-type '(unsigned-byte 8)
                                  :initial-contents bytes))))

(defun parse-length (line)
  (/ (parse-integer (subseq line 6 (- (length line) 1))) 4))

(defun parse-message (line length)
  (parse-hex-string
   (subseq line 6 (+ 6 length))))

(defun parse-digest (line start digest-length)
  (parse-hex-string
   (subseq line start (+ start (/ digest-length 4)))))

;;;; Tests

(define-test-suite (clean-sha3))

(macrolet
    ((sha3-case (name type digest-length)
       (let ((filename (format nil "tests/SHA3_~A~AMsg.rsp" digest-length type)))
         `(define-test-case (,(symbolicate type "-MESSAGES"))
            (with-open-file (stream ,filename)
              (dotimes (i 8) (read-line stream))
              (let-while (line (read-line stream nil))
                (let* ((length (parse-length line))
                       (message (parse-message (read-line stream) length))
                       (digest (parse-digest (read-line stream) 5 ,digest-length)))
                  (assert-that (equalp digest (,name message)))
                  (read-line stream nil)))))))
     (sha3-monte-case (name digest-length)
       `(define-test-case (monte-carlo)
          (with-open-file (stream (format nil "tests/SHA3_~AMonte.rsp" ,digest-length))
            (dotimes (i 8) (read-line stream))
            (let ((digest (parse-digest (read-line stream) 7 ,digest-length)))
              (dotimes (i 100)
                (dotimes (i 1000)
                  (setf digest (,name digest)))
                (read-line stream)
                (read-line stream)
                (assert (equalp digest (parse-digest (read-line stream) 5 ,digest-length))))))))
     (sha3-suite (digest-length)
       (let ((name (find-symbol (format nil "SHA3-~A" digest-length) :clean-sha3)))
         `(with-test-suite (,name :parent clean-sha3)
            (sha3-case ,name "Short" ,digest-length)
            (sha3-case ,name "Long" ,digest-length)
            (sha3-monte-case ,name ,digest-length)))))

  (sha3-suite 224)
  (sha3-suite 256)
  (sha3-suite 384)
  (sha3-suite 512))
